# Bughunting Challenge

Show the world that you are a great hacker!

Bughunting is a Challenge in which you fix bugs in various applications. It imitates part of the workday of a Linux distribution developer. We prepared for you a couple of tasks in various programming languages. Available languages include C, Python, Perl, Shell, JavaScript, and Java. We hope that anyone can find something they like.

The purpose of every task is to find the bug, and fix it in the code. Your solution is evaluated in real-time after you submit it, and you score points if your solution is correct. Some of the issues are artificial and were created only for the Challenge, while some are real-life issues.

## Watch a Quick Introduction on YouTube

[![Bughunting Challenge Introduction for DevConf.CZ](http://img.youtube.com/vi/GvG-4nYUV-c/0.jpg)](http://www.youtube.com/watch?v=GvG-4nYUV-c "Bughunting Challenge Introduction for DevConf.CZ")

If you like this kind of puzzles and would like to do this for the living, talk to our Talent acquisition team that is available during the DevConf.CZ event on <http://bit.ly/bughunting-ta>.

## Challenge organization

Challenge is organized into day-long sessions. Sessions consist of tasks with various difficulty. More difficult tasks are for more points. We advise you to start with the easier ones, as more advanced tasks can be quite tricky.

## Rules

* **DevConf.CZ Registration is required before the Challenge in Hopin: <https://hopin.com/events/devconf-cz-2021>**
* You'll get the Codename in our Hopin session: <https://app.hopin.com/events/devconf-cz-2021/sessions/1d433302-0c53-420b-aeda-3b23687c09d2>.
* You can attend the Challenge only once!
* Tasks have **hints** to help you find the issue. Each time you uncover a hint, the system lowers the maximum number of points you can get for the task.
* The solution can be a dirty fix, but you must not break the basic functionality of the application. Feel free to test how good our tests are for submitted solutions ;)
* Winners are announced at the conference end.

## Setting up the environment

* A quick intro how the Challenge works is available at <https://youtu.be/GvG-4nYUV-c>.
* If you have any troubles setting up the environment, reach out to us in the Hopin session.
* Linux containers are used for the Challenge environment, therefore you need to install the container runtime.
* Please, **prepare the environment ahead**. Pulling the container image takes some time, and you can try working with the container before the Challenge.
* We recommend to use `podman`, but it should work fine with `docker` as well.

1. Depending on the operating system you use, install the `podman` or `docker`:
   * <https://docs.docker.com/get-docker>
     * For the `docker` variant, run the docker daemon (command for Fedora below; for other OS, see the documentation):
    ```console
    sudo systemctl start docker
    ```

   * <https://podman.io/getting-started/installation.html>
     * We use docker command below, but if you didn’t install podman-docker sub-package (which adds a symlink `docker` -> `podman`), then it should simply work if you replace `docker` with `podman` in all the commands below


2. Before the Challenge starts, you can use Codename `test`, key `test`, and go to <http://www.bughunting.cz> to login (leave the Key empty).
   * This is useful to have the base container cached, and also to investigate how to solve the tasks.

3. When the Challenge starts, get a Codename from us in the Hopin session, and go to <http://www.bughunting.cz>. It will also show you specific commands to pull, and run the container.
   * If you need to logout, to use different Codename, go to <https://bughunting.cz/logout/>

4. Follow the setup commands on the welcome screen.
   * **After login you will see the specific commands to pull and run the container.**
   * If you want to know what the commands you're running do, see the corresponding section bellow.
   * You need to re-run the commands if you've run them with different Codename previously.

## Solve tasks

**You need to have the container running.**

Now, the following commands are run inside the container:

1. You can list the directories with tasks:
    ```
    [bughunting] $ ls
    warm_up
    ```

2. For working on a task, you need to be inside the task directory
    ```
    [bughunting] $ cd warm_up
    ```

3. Edit the source code

   You can edit the file either inside the container (Press CTRL+X to exit the `nano` editor):
    ```
    [bughunting] $ nano script.sh
    ```

   Or you can edit the file in your `bughunting-tasks` directory on your host (this does not work with `docker` without further configuration, it works by default with `podman`).

4. Once happy with the solution, submit the task for evaluation:
    ```
    [bughunting] $ hunt
    ```

5. You see immediately whether you scored or not
    ```
    OK: You gain 1 score points.
    ```

## Where are the tasks

As seen in the example above, in the container, the directory **/home/bughunting** contains a directory named **sources**. This directory contains directories with names of the tasks. When you open the directory with one of the task names, you’ll see all the sources. **Follow the steps to reproduce a failure that is described on the website.**

Please note that the tasks are usually artificial and the solutions are pretty easy. Don’t spend too much time on one task - if you struggle, there are other tasks that can be a better fit for you. Also consider using task hints if you are stuck.

## How to submit your fix

To submit the task for verification, run the command `hunt` in the terminal, inside the task directory in the container. You can submit every task as many times as you want.

```console
hunt check warm_up
```


## What the commands on welcome-screen do? **For reference only.**

3. Pull the container.
    ```console
    docker pull quay.io/bughunting/client
    ```
    Pulling the image may take some time, for the first time. Subsequent runs will be faster.

4. Prepare an empty working directory, which you will bind-mount into the container:
    ```console
    mkdir bughunting-tasks
    ```
    The tasks will get installed into this directory.

5. Run the container, bind-mounting the `bughunting-tasks` directory into the container.
    ```console
    docker run -e KEY=??? -d -v $(pwd)/bughunting-tasks:/home/bughunting/sources:z,rw --name bughunting quay.io/bughunting/client
    ```
    You need to use the KEY provided in web interface, after login.

6. Connect to the running container:
    ```console
    docker exec -ti bughunting zsh
    ```

## F&Q

1. **Question: I want to start the container and see the following error, what should I do?**

       Error: error creating container storage: the container name "bughunting"
       is already in use by "63e4f8f69068f2121ab7b6ec261b7cb89c51fd68c6b3cc462786d767bcd2dee5".
       You have to remove that container to be able to reuse that name.: that
       name is already in use

    **Answer:** You already have created a container before. Use `docker -ti exec bughunting bash` to attach to that existing container.

2. **Question: I'm getting the following error when I'm pulling the image, what should I do?**

       $ docker run -e KEY=... -d --pull always --name bughunting quay.io/bughunting/client
       Emulate Docker CLI using podman. Create /etc/containers/nodocker to quiet msg.
       Trying to pull quay.io/bughunting/client...
         Get https://d3uo42mtx6z2cr.cloudfront.net/sha256/cd/cd4916bc5314ba8615a141133fbe9872dfe2e1dcfbab66e6e2ec34f5ba298121?Expires=1586934124&Signature=bqPl9sRMYa8I3FsI8oe20LDsU8p6jtadMfKS~RiyXoY98IjlA~tf0DPMnJokpApMuu91L14Gg0iBn9yiXVtFXzaJlJNRpf75~ak3MdG1MyTGRLOUK~MM34A1ZVe6mTYZ9sb4cnsEFRr8min5S6isTgQ6q4rRJFF2qJcB1cHJ2YD6s5h0plcrXvYgIwmzSwluutgkIwOpABxVt-ykTVpTt8~HHcBEKBr-FQaZuwHPPJSGbfGwUtMGVgTLb3Pl7hlf-Y~KSf2cvS7sQepxRrZ5rgYtTYMFtlw08eZ3o8yKEgLODSh0D54KRCBOiz3dgkwI9J-gC5igPXB-JBnJWu-qxw__&Key-Pair-Id=APKAJ67PQLWGCSP66DGA: dial tcp: lookup d3uo42mtx6z2cr.cloudfront.net on 127.0.0.1:53: server misbehaving
       Error: unable to pull quay.io/bughunting/client: unable to pull image: Error parsing image configuration: Get https://d3uo42mtx6z2cr.cloudfront.net/sha256/cd/cd4916bc5314ba8615a141133fbe9872dfe2e1dcfbab66e6e2ec34f5ba298121?Expires=1586934124&Signature=bqPl9sRMYa8I3FsI8oe20LDsU8p6jtadMfKS~RiyXoY98IjlA~tf0DPMnJokpApMuu91L14Gg0iBn9yiXVtFXzaJlJNRpf75~ak3MdG1MyTGRLOUK~MM34A1ZVe6mTYZ9sb4cnsEFRr8min5S6isTgQ6q4rRJFF2qJcB1cHJ2YD6s5h0plcrXvYgIwmzSwluutgkIwOpABxVt-ykTVpTt8~HHcBEKBr-FQaZuwHPPJSGbfGwUtMGVgTLb3Pl7hlf-Y~KSf2cvS7sQepxRrZ5rgYtTYMFtlw08eZ3o8yKEgLODSh0D54KRCBOiz3dgkwI9J-gC5igPXB-JBnJWu-qxw__&Key-Pair-Id=APKAJ67PQLWGCSP66DGA: dial tcp: lookup d3uo42mtx6z2cr.cloudfront.net on 127.0.0.1:53: server misbehaving

    **Answer:** From time to time, there are some issues with quay.io. Just keep trying and the command will eventually succeed.

3. **Question: I ran the container with a wrong key. What should I do?**

    **Answer:** Kill and remove the container by `docker kill bughunting ; docker rm bughunting`. Then, run it again with a correct key.


4. **Question: I would like to use gdb (or any other tool for debugging). What should I do?**

    **Answer:** You can install further packages in the container. For example, gdb can be installed using `dnf install gdb`.

